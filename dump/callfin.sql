/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100130
Source Host           : localhost:3306
Source Database       : callfin

Target Server Type    : MYSQL
Target Server Version : 100130
File Encoding         : 65001

Date: 2018-08-03 17:35:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for authors
-- ----------------------------
DROP TABLE IF EXISTS `authors`;
CREATE TABLE `authors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of authors
-- ----------------------------
INSERT INTO `authors` VALUES ('1', 'Джон Рональд Руел Толкиен', null, null);
INSERT INTO `authors` VALUES ('2', 'Джордж Мартин', null, null);
INSERT INTO `authors` VALUES ('3', 'Федор Достоевский', null, null);
INSERT INTO `authors` VALUES ('4', 'Лев Толстой', null, null);

-- ----------------------------
-- Table structure for books
-- ----------------------------
DROP TABLE IF EXISTS `books`;
CREATE TABLE `books` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(10) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_id` int(10) unsigned DEFAULT NULL,
  `publishing_house_id` int(10) unsigned NOT NULL,
  `publishing_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `books_author_id_foreign` (`author_id`),
  KEY `books_image_id_foreign` (`image_id`),
  KEY `books_publishing_house_id_foreign` (`publishing_house_id`),
  CONSTRAINT `books_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`),
  CONSTRAINT `books_image_id_foreign` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`),
  CONSTRAINT `books_publishing_house_id_foreign` FOREIGN KEY (`publishing_house_id`) REFERENCES `publishing_houses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of books
-- ----------------------------
INSERT INTO `books` VALUES ('1', '3', 'Идиот', null, '3', '1869-08-03', null, null);
INSERT INTO `books` VALUES ('2', '2', 'Песнь льда и пламени', null, '2', '1996-08-03', null, null);
INSERT INTO `books` VALUES ('3', '1', 'Властелин колец', null, '1', '2001-08-03', null, null);
INSERT INTO `books` VALUES ('4', '4', 'Война и мир', null, '3', '1867-08-03', null, null);

-- ----------------------------
-- Table structure for images
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of images
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('53', '2018_08_02_195012_create_authors_table', '1');
INSERT INTO `migrations` VALUES ('54', '2018_08_02_195146_create_publishing_houses_table', '1');
INSERT INTO `migrations` VALUES ('55', '2018_08_02_195533_create_images_table', '1');
INSERT INTO `migrations` VALUES ('56', '2018_08_02_195623_create_books_table', '1');

-- ----------------------------
-- Table structure for publishing_houses
-- ----------------------------
DROP TABLE IF EXISTS `publishing_houses`;
CREATE TABLE `publishing_houses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of publishing_houses
-- ----------------------------
INSERT INTO `publishing_houses` VALUES ('1', 'Абабагаламага', null, null);
INSERT INTO `publishing_houses` VALUES ('2', 'Буква', null, null);
INSERT INTO `publishing_houses` VALUES ('3', 'Питер', null, null);
SET FOREIGN_KEY_CHECKS=1;
