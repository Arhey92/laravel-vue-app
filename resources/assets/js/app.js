
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
const components = require.context('./components', true, /\.vue$/);

components.keys().forEach(k => Vue.component(`${getModuleName(k)}`, require(`./components/${getModuleName(k)}.vue`)));

function getModuleName (path) {
    return path.slice(path.lastIndexOf('/') + 1, path.lastIndexOf('.'))
}

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//example
// Vue.component('container', require('./components/Container.vue'));

const app = new Vue({
    el: '#app'
});
