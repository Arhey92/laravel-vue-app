<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();

        foreach($books as $book){
            $book->author;
            $book->publishingHouse;
            $book->image;
        }

        return response()->json($books);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'author_id' => 'required|integer',
            'title' => 'required|string',
            'publishing_house_id' => 'required|integer',
            'publishing_date' => 'required|date'
        ]);

        if ($validator->fails()) {
            return response()->json($validator, 422);
        }

        $book = Book::create([
            'author_id' => $request->author_id,
            'title' => $request->title,
            'publishing_house_id' => $request->publishing_house_id,
            'publishing_date' => $request->publishing_date
        ]);

        return response()->json($book, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);

        if(!is_null($book->image_id)){
            Storage::disk('public')->delete($book->image->path.$book->image->title);
        }

        $book->delete();

        return response()->json(['success' => 'Book was successfully deleted from storage.'], 204);
    }
}
