<?php

namespace App\Http\Controllers;

use App\Book;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'book_id' => 'required|integer',
            'image' => 'required'
        ]);

        $image = $request->image;

        $title = $request->book_id.'_'.$request->file('image')->getClientOriginalName();
        $path = '/images/';
        $extension = 'jpg';

        DB::beginTransaction();

        try {
            $media = new Image();
            $storedImage = $media->createNewImage($title, $path, $extension);

            Storage::disk('public')->putFileAs($path, $request->file('image'), $title, 'public');
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['errors' => 'Storing image was failed.'], 400);
        } finally {
            $book = Book::find($request->book_id);
            $book->update([
                'image_id' => $storedImage->id
            ]);
        }

        DB::commit();

        return response()->json($book, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $image = Image::find($id);

        return $image;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getImageUrl($id){
        $image = Image::find($id);

        return response()->json(["image_url" => asset("storage".$image->path.$image->title)]);
    }
}
