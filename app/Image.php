<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
        'title',
        'extension',
        'path'
    ];

    public function createNewImage($title, $path, $extension){
        return self::create([
            'title' => $title,
            'extension' => $extension,
            'path' => $path
        ]);
    }
}
