<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
       'author_id',
       'title',
       'image_id',
       'publishing_house_id',
       'publishing_date'
    ];

    public function author()
    {
        return $this->hasOne('App\Author', 'id', 'author_id');
    }

    public function image()
    {
        return $this->hasOne('App\Image', 'id', 'image_id');
    }

    public function publishingHouse()
    {
        return $this->hasOne('App\PublishingHouse', 'id', 'publishing_house_id');
    }
}
