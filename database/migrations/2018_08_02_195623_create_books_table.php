<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('author_id');
            $table->string('title');
            $table->unsignedInteger('image_id')->nullable();
            $table->unsignedInteger('publishing_house_id');
            $table->date('publishing_date');
            $table->timestamps();

            $table->foreign('author_id')->references('id')->on('authors');
            $table->foreign('image_id')->references('id')->on('images');
            $table->foreign('publishing_house_id')->references('id')->on('publishing_houses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
