<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
                'author_id' => 3,
                'title' => 'Идиот',
                'publishing_house_id' => 3,
                'publishing_date' => \Carbon\Carbon::create(1869)
            ],
            [
                'author_id' => 2,
                'title' => 'Песнь льда и пламени',
                'publishing_house_id' => 2,
                'publishing_date' => \Carbon\Carbon::create(1996)
            ],
            [
                'author_id' => 1,
                'title' => 'Властелин колец',
                'publishing_house_id' => 1,
                'publishing_date' => \Carbon\Carbon::create(2001)
            ],
            [
                'author_id' => 4,
                'title' => 'Война и мир',
                'publishing_house_id' => 3,
                'publishing_date' => \Carbon\Carbon::create(1867)
            ]
        ]);
    }
}
