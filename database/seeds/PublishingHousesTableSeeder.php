<?php

use Illuminate\Database\Seeder;

class PublishingHousesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('publishing_houses')->insert([
            [
                'name' => 'Абабагаламага'
            ],
            [
                'name' => 'Буква'
            ],
            [
                'name' => 'Питер'
            ]
        ]);
    }
}
