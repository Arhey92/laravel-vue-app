<?php

use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authors')->insert([
            [
                'name' => 'Джон Рональд Руел Толкиен'
            ],
            [
                'name' => 'Джордж Мартин'
            ],
            [
                'name' => 'Федор Достоевский'
            ],
            [
                'name' => 'Лев Толстой'
            ]
        ]);
    }
}
