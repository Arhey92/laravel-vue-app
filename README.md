##Start project

- Create .env file from .env.example
- Next call command
```text
    composer update
```
- Next you must generate your app key
```text
    php artisan key:generate
```
- Than you need autoload classes
```text
    composer dump-autoload
```
- (if you dont use dump database) run command to create all tables and fill them start test data.
```text
    php artisan migrate --seed
```
- At least for correct store images you must run next command
```text
    php artisan storage:link
```
- Install npm
```text
    npm install
```
- Building front
```text
    npm run dev
```
- At least run front part
```text
    npm run watch
```
- Testing app

### Important!
If you want to use dump of my database ino your local DB, you dont need to do 5 step, only 1-4 and 6.